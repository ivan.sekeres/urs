/*
 * CS43L22.c
 *
*/

#include "CS43L22.h"

void CS43L22_Init(void)
{
	GPIO_InitTypeDef ResetPinInit;
	GPIO_InitTypeDef I2CInitPinStruct;
	GPIO_InitTypeDef I2SInitPinStruct;
	GPIO_InitTypeDef I2SWSPinInit;
	I2S_InitTypeDef I2SInitStruct;
	I2C_InitTypeDef I2CInitStruct;

	uint8_t CS43L22CommandBuffer[5];
	volatile unsigned int counter = 1000000;

	///enable clock (I2C, SPI, Audio_reset, Audio_DAC)
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOD, ENABLE);

	//Reset pin configuration - PD4
	ResetPinInit.GPIO_Pin = RESET_PIN;
	ResetPinInit.GPIO_Mode = GPIO_Mode_OUT;
	ResetPinInit.GPIO_PuPd = GPIO_PuPd_DOWN; //ovo nije potrebno jer je na plocici 10k
	ResetPinInit.GPIO_OType = GPIO_OType_PP;
	ResetPinInit.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &ResetPinInit);

	//turn off CS43L22
	GPIO_ResetBits(GPIOD, RESET_PIN);

	//I2C pins config - PB9 and PB6
	I2CInitPinStruct.GPIO_Pin = I2C_SCL_PIN | I2C_SDA_PIN;
	I2CInitPinStruct.GPIO_Mode = GPIO_Mode_AF;
	I2CInitPinStruct.GPIO_OType = GPIO_OType_OD;
	I2CInitPinStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	I2CInitPinStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &I2CInitPinStruct);

	//connect GPIO pins to I2C peripheral
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1);

	// I2S pins config - PC7, PC10, PC12 and PA4
	I2SInitPinStruct.GPIO_Pin = I2S3_SCLK_PIN | I2S3_SD_PIN | I2S3_MCLK_PIN;
	I2SInitPinStruct.GPIO_Mode = GPIO_Mode_AF;
	I2SInitPinStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOC, &I2SInitPinStruct);

	I2SWSPinInit.GPIO_Pin = I2S3_WS_PIN;
	I2SWSPinInit.GPIO_Mode = GPIO_Mode_AF;
	I2SWSPinInit.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOA, &I2SWSPinInit);

	//I2S and I2C clock enable
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1 | RCC_APB1Periph_SPI3, ENABLE);
	RCC_PLLI2SCmd(ENABLE);

	//connect GPIO pins to SPI peripheral
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_SPI3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_SPI3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3);

	//I2S config
	SPI_I2S_DeInit(CS43L22_I2S);
	I2SInitStruct.I2S_AudioFreq = I2S_AudioFreq_48k;
	I2SInitStruct.I2S_MCLKOutput = I2S_MCLKOutput_Enable;
	I2SInitStruct.I2S_DataFormat = I2S_DataFormat_16b;
	I2SInitStruct.I2S_Mode = I2S_Mode_MasterTx;
	I2SInitStruct.I2S_Standard = I2S_Standard_Phillips;
	I2SInitStruct.I2S_CPOL = I2S_CPOL_Low;
	I2S_Init(CS43L22_I2S, &I2SInitStruct);

	//I2C config
	I2C_DeInit(CS43L22_I2C);
	I2CInitStruct.I2C_ClockSpeed = 100000;
	I2CInitStruct.I2C_Mode = I2C_Mode_I2C;
	I2CInitStruct.I2C_OwnAddress1 = CS43L22_CORE_I2C_ADDRESS;
	I2CInitStruct.I2C_Ack = I2C_Ack_Enable;
	I2CInitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2CInitStruct.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_Init(CS43L22_I2C, &I2CInitStruct);
	I2C_Cmd(CS43L22_I2C, ENABLE);

	//bring CS43L22 online
	GPIO_SetBits(GPIOD, RESET_PIN);
	while(counter)
		counter--;

	//startup sequence, see recommended power up sequence CS43L22 datasheet pp.32

	//Write 0x01 to register "Power Ctl 1" - power-down mode
	CS43L22CommandBuffer[0] = CS43L22_POWER_CTL_1;
	CS43L22CommandBuffer[1] = 0x01;
	CS43L22_register_write(CS43L22CommandBuffer, 2);

	//load the desired register settings
	CS43L22_desired_settings();

	//load the required register settings
	CS43L22_required_settings();

	//set the “Power Ctl 1” register (0x02) to 0x9E
	CS43L22CommandBuffer[0] = CS43L22_POWER_CTL_1;
	CS43L22CommandBuffer[1] = 0x9E;
	CS43L22_register_write(CS43L22CommandBuffer, 2);

	I2S_Cmd(CS43L22_I2S, ENABLE);
}


void CS43L22_required_settings(void)
{
	uint8_t regValue = 0xFF;
	uint8_t cmdBuffer[5];

	//Write 0x99 to register 0x00
	cmdBuffer[0] = 0x00;
	cmdBuffer[1] = 0x99;
	CS43L22_register_write(cmdBuffer, 2);

	//Write 0x80 to register 0x47
	cmdBuffer[0] = 0x47;
	cmdBuffer[1] = 0x80;
	CS43L22_register_write(cmdBuffer, 2);

	//Write ‘1’b to bit 7 in register 0x32
	regValue = CS43L22_register_read(0x32);
	cmdBuffer[0] = 0x32;
	cmdBuffer[1] = regValue | 0x80;
	CS43L22_register_write(cmdBuffer, 2);

	//Write ‘0’b to bit 7 in register 0x32
	regValue = CS43L22_register_read(0x32);
	cmdBuffer[0] = 0x32;
	cmdBuffer[1] = regValue & (~0x80);
	CS43L22_register_write(cmdBuffer, 2);

	//Write 0x00 to register 0x00
	cmdBuffer[0] = 0x00;
	cmdBuffer[1] = 0x00;
	CS43L22_register_write(cmdBuffer, 2);
}

void CS43L22_desired_settings(void)
{
	uint8_t cmdBuffer[5];

	//All “Reserved” bits in registers must maintain their default value!

	//headphone channels (A and B) always on, speaker channels (A and B) always off
	cmdBuffer[0] = CS43L22_POWER_CTL_2;
	cmdBuffer[1] = 0xAF;
	CS43L22_register_write(cmdBuffer, 2);

	//Headphone Analog Gain
	cmdBuffer[0] = CS43L22_PLAYBACK_CTL_1;
	cmdBuffer[1] = 0x70;
	CS43L22_register_write(cmdBuffer, 2);

	//clock control - auto
	cmdBuffer[0] = CS43L22_CLOCKING_CTL;
	cmdBuffer[1] = 0x80;
	CS43L22_register_write(cmdBuffer, 2);

	// interface control - I²S, 16-bit data
	cmdBuffer[0] = CS43L22_INTERFACE_CTL_1;
	cmdBuffer[1] = 0x07;
	CS43L22_register_write(cmdBuffer, 2);

	//passthrough A - use AIN1A
	cmdBuffer[0] = CS43L22_PASSTHROUGH_A_SELECT;
	cmdBuffer[1] = 0x81;
	CS43L22_register_write(cmdBuffer, 2);

	//passthrough B - use AIN1B
	cmdBuffer[0] = CS43L22_PASSTHROUGH_B_SELECT;
	cmdBuffer[1] = 0x81;
	CS43L22_register_write(cmdBuffer, 2);

	//enable analog passthrough; mute disabled
	cmdBuffer[0] = CS43L22_MISC_CTL;
	cmdBuffer[1] = 0xC0;
	CS43L22_register_write(cmdBuffer, 2);

	//unmute headphone and speaker
	cmdBuffer[0] = CS43L22_PLAYBACK_CTL_2;
	cmdBuffer[1] = 0x00;
	CS43L22_register_write(cmdBuffer, 2);
}


void Passthrough_Volume_Low()
{
	uint8_t cmdBuffer[5];

	cmdBuffer[0] = CS43L22_PASSTHROUGH_A_VOL | 0x80;
	cmdBuffer[1] = 0xB0;
	cmdBuffer[2] = 0xB0;
	CS43L22_register_write(cmdBuffer, 3);
}

void Passthrough_Volume_High()
{
	uint8_t cmdBuffer[5];

	cmdBuffer[0] = CS43L22_PASSTHROUGH_A_VOL | 0x80;
	cmdBuffer[1] = 0x00;
	cmdBuffer[2] = 0x00;
	CS43L22_register_write(cmdBuffer, 3);
}


void CS43L22_register_write(uint8_t controlBytes[], uint8_t numBytes)
{
	uint8_t bytesSent=0;

	while (I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_BUSY))
	{
		//just wait until no longer busy
	}

	I2C_GenerateSTART(CS43L22_I2C, ENABLE);
	while (!I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_SB))
	{
		//wait for generation of start condition
	}
	I2C_Send7bitAddress(CS43L22_I2C, CS43L22_I2C_ADDRESS, I2C_Direction_Transmitter);
	while (!I2C_CheckEvent(CS43L22_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
	{
		//wait for end of address transmission
	}
	while (bytesSent < numBytes)
	{
		I2C_SendData(CS43L22_I2C, controlBytes[bytesSent]);
		bytesSent++;
		while (!I2C_CheckEvent(CS43L22_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
		{
			//wait for transmission of byte
		}
	}
	while(!I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_BTF))
	{
	    //wait until it's finished sending before creating STOP
	}
	I2C_GenerateSTOP(CS43L22_I2C, ENABLE);
}


uint8_t CS43L22_register_read(uint8_t mapbyte)
{
	uint8_t receivedByte = 0;

	while (I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_BUSY))
	{
		//just wait until no longer busy
	}

	I2C_GenerateSTART(CS43L22_I2C, ENABLE);
	while (!I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_SB))
	{
		//wait for generation of start condition
	}

	I2C_Send7bitAddress(CS43L22_I2C, CS43L22_I2C_ADDRESS, I2C_Direction_Transmitter);
	while (!I2C_CheckEvent(CS43L22_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
	{
		//wait for end of address transmission
	}

	I2C_SendData(CS43L22_I2C, mapbyte); //sets the transmitter address
	while (!I2C_CheckEvent(CS43L22_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
	{
		//wait for transmission of byte
	}

	I2C_GenerateSTOP(CS43L22_I2C, ENABLE);

	while (I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_BUSY))
	{
		//just wait until no longer busy
	}

	I2C_AcknowledgeConfig(CS43L22_I2C, DISABLE);

	I2C_GenerateSTART(CS43L22_I2C, ENABLE);
	while (!I2C_GetFlagStatus(CS43L22_I2C, I2C_FLAG_SB))
	{
		//wait for generation of start condition
	}

	I2C_Send7bitAddress(CS43L22_I2C, CS43L22_I2C_ADDRESS, I2C_Direction_Receiver);
	while (!I2C_CheckEvent(CS43L22_I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
	{
		//wait for end of address transmission
	}

	while (!I2C_CheckEvent(CS43L22_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED))
	{
		//wait until byte arrived
	}
	receivedByte = I2C_ReceiveData(CS43L22_I2C);

	I2C_GenerateSTOP(CS43L22_I2C, ENABLE);

	return receivedByte;
}




