/*
******************************************************************************
File:     main.c
Info:     Generated by Atollic TrueSTUDIO(R) 9.3.0   2024-06-03

The MIT License (MIT)
Copyright (c) 2019 STMicroelectronics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************************
*/

/* Includes */

#include "stm32f4xx.h"

/* task 1
void TIM1_BRK_TIM9_IRQHandler(void)
{
	GPIO_ToggleBits(GPIOD, GPIO_Pin_15);

	TIM_ClearITPendingBit(TIM9, TIM_IT_Update);
}

void TIM2_IRQHandler(void)
{
	GPIO_ToggleBits(GPIOD, GPIO_Pin_14);

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
}
*/

/* task 2
volatile int duty_cycle_toggle = 0;
volatile float duty_cycle = 1;

void TIM2_IRQHandler(void)
{
	if(duty_cycle_toggle == 0)
	{
		if(duty_cycle < 10)
			duty_cycle++;
		else
			duty_cycle_toggle = !duty_cycle_toggle;
	}

	if(duty_cycle_toggle == 1)
	{
		if(duty_cycle > 1)
			duty_cycle--;
		else
			duty_cycle_toggle = !duty_cycle_toggle;
	}

	TIM4->CCR3 = (int)(42000*duty_cycle/10.0) - 1;

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
}
*/




int main(void)
{
/* task 1
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15 | GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9, ENABLE);

	//podešavanje TIM9 - UEV svakih 1000 ms
	TIM_TimeBaseInitTypeDef TIM9InitStructure;
	TIM9InitStructure.TIM_Prescaler = 3359; //vrijednost dijelitelja frekvencije
	TIM9InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM9InitStructure.TIM_Period = 49999; //vrijednost auto-reload registra
	TIM9InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM9, &TIM9InitStructure);
	TIM_ITConfig(TIM9, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM9, ENABLE);
	//postavi prioritet TIM9 prekida
	NVIC_SetPriority(TIM1_BRK_TIM9_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 1, 0));
	//omogući TIM9 prekid
	NVIC_EnableIRQ(TIM1_BRK_TIM9_IRQn);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	//podešavanje TIM2 - UEV svakih 250 ms
	TIM_TimeBaseInitTypeDef TIM2InitStructure;
	TIM2InitStructure.TIM_Prescaler = 3359; //vrijednost dijelitelja frekvencije
	TIM2InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM2InitStructure.TIM_Period = 6249; //vrijednost auto-reload registra
	TIM2InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM2, &TIM2InitStructure);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2, ENABLE);

	//postavi prioritet TIM2 prekida
	NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 1, 1));
	//omogući TIM2 prekid
	NVIC_EnableIRQ(TIM2_IRQn);
*/

///* task 2
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	//podešavanje TIM2 - UEV svakih 250 ms
	TIM_TimeBaseInitTypeDef TIM2InitStructure;
	TIM2InitStructure.TIM_Prescaler = 3359; //vrijednost dijelitelja frekvencije
	TIM2InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM2InitStructure.TIM_Period = 5000; //vrijednost auto-reload registra
	TIM2InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM2, &TIM2InitStructure);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2, ENABLE);

	//postavi prioritet TIM2 prekida
	NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 1, 0));
	//omogući TIM2 prekid
	NVIC_EnableIRQ(TIM2_IRQn);

	//uključivanje takta za port D
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	//koristi se alternativna funkcija pina PD14
	GPIO_InitTypeDef GPIO_InitStructPWM;
	GPIO_InitStructPWM.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructPWM.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructPWM.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructPWM.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructPWM.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructPWM);
	// alternativna funkcija pina PD14 je PWM signal s TIM4
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_TIM4);
	//omogući takt za TIM4
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	//podešavanje TIM4 vremenske baze - frekvencija PWM signala je 2 kHz
	TIM_TimeBaseInitTypeDef TIM4_InitStruct;
	TIM4_InitStruct.TIM_Prescaler = 0;
	TIM4_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM4_InitStruct.TIM_Period = 41999;
	TIM4_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM4, &TIM4_InitStruct);
	TIM_Cmd(TIM4, ENABLE);
	//podešavanje karakteristika PWM signala
	TIM_OCInitTypeDef TIM4_OCInitStruct;
	TIM4_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM4_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM4_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM4_OCInitStruct.TIM_Pulse = 0; //početna vrijednost duty-cyclea je 0%
	TIM_OC3Init(TIM4, &TIM4_OCInitStruct); //PWM signal na CH3
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
	//TIM4->CCR3 = (int)(42000*0.1) - 1; //duty-cycle iznosi 10%
	//TIM4->CCR3 = (int)(42000*0.5) - 1; //duty-cycle iznosi 50%
//*/

	while (1)
	{

	}
}
